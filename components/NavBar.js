import React, {useContext} from 'react';
import {Navbar, Nav} from 'react-bootstrap';
import Link from 'next/link';
import UserContext from './../UserContext.js';

export default function NavBar(){
	const {user} = useContext(UserContext);
	// console.log(user)
	return (
		<React.Fragment>
              <Navbar bg="dark" expand="lg" variant="dark">
              	<Link href={user.id !== null ? "/records" : "/"}>
              		<a className="navbar-brand">Budget Tracker</a>
              	</Link>
              	<Navbar.Toggle aria-controls="basic-navbar-nav" />
              	<Navbar.Collapse id="basic-navbar-nav">
              		{user.id !== null
              			  ? <React.Fragment>
              		  	    	<Nav className="mr-auto">
              		  	      	    <Link href="/categories">
              		  	    			<a className="nav-link" role="button">Categories</a>
              		  	    		</Link>
              		  	    		<Link href="/records">
              		  	    			<a className="nav-link" role="button">Records</a>
              		  	    		</Link>
              		  	    		<Link href="/monthly">
              		  	    			<a className="nav-link" role="button">Monthly Chart</a>
              		  	    		</Link>
              		  	    	</Nav>
              		  	    	<Nav className="ml-auto">
              		  	    		<Link href="/logout">
              		  	    			<a className="nav-link" role="button">Logout</a>
              		  	    		</Link>
              		  	    	</Nav>
              			    </React.Fragment>
              			  : null
              		}
              	</Navbar.Collapse>
              </Navbar>
		</React.Fragment>
		)
}
