import React, {useState, useEffect, useContext} from 'react';
import Router from 'next/router';
import {Form, Button, Card, Alert} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function addCategory(){
	const [name, setName] = useState('');
	const [type, setType] = useState('');

	function create(e){
		e.preventDefault();
		fetch('https://hidden-temple-78735.herokuapp.com/api/categories/', {
		    method: "POST",
		    headers: {
		        'Content-Type': "application/json",
		        'Authorization': `Bearer ${localStorage.getItem('token')}`
		    },
		    body: JSON.stringify({
		        name: name,
		        type: type
		    })
		})
		.then( res => res.json())
		.then(data => {
			// console.log(data)
			if(data === false){
				Swal.fire({
				    icon: "error",
				    title: "Category already exist",
				    text: "Please use a different category name/type"
				})
			}else{
				Swal.fire({
				    icon: "success",
				    title: "Category has been created",
				    text: `Name: "${name}" | Type: "${type}" `
				})
				Router.push('/categories')
			}
		})
		setName('')
		setType('')
	}

	return (
		<React.Fragment>
			<div className="row d-flex justify-content-center mt-5">
                <div className="col-sm-12 col-md-10 col-lg-8">
	                <Alert variant="warning">
	                  <Alert.Heading>New Record</Alert.Heading>
	                </Alert>
	                <Alert variant="info">
	                    <Alert.Heading>Category Information</Alert.Heading>
	                    <hr />
                    	<Form onSubmit={(e) => create(e)}>
                    		<Form.Group controlId="categoryName">
                    		  <Form.Label>Category Name:</Form.Label>
                    		  <Form.Control type="text" placeholder="Enter category name" value={name} onChange={(e) => setName(e.target.value)} required/>
                    		</Form.Group>
                    		<Form.Group controlId="categoryType">
                    		  <Form.Label>Category Type:</Form.Label>
                    		  <Form.Control as="select" value={type} onChange={(e) => setType(e.target.value)} required>
                    		  	  <option>Select Category</option>
                    		      <option>Income</option>
                    		      <option>Expense</option>
                    		  </Form.Control>
                    		</Form.Group>
                    		<Button variant="primary btn-block" type="submit" id="submitBtn">Submit</Button>
                    	</Form>
	                </Alert>
                </div>
			</div>
		</React.Fragment>

		)
}
