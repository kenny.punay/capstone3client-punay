import React, {useState, useEffect, useContext} from 'react';
import Router from 'next/router';
import BarChart from './BarChart.js';
import moment from 'moment';
import {Alert} from 'react-bootstrap';


export default function Expense(){
	const [allRecords, setAllRecords] = useState([]);
	const [description, setDescription] = useState([]);
	const [type, setType] = useState([]);
	const [date, setDate] = useState([]);
	const [amount, setAmount] = useState([]);

	useEffect(()=> {
		fetch('https://hidden-temple-78735.herokuapp.com/api/ledger/', {
		    headers: {
		        'Authorization': `Bearer ${localStorage.getItem('token')}`
		    }
		})
		.then( res => res.json())
		.then(data => {
			setAllRecords(data)
		})
	}, [])
	// console.log(amount)

	return (
		<React.Fragment>
			<div className="row d-flex justify-content-center mt-5">
                <div className="col-sm-12 col-md-10 col-lg-8">
                	<Alert variant="warning">
                	  <Alert.Heading className="text-center">Monthly Balance Chart</Alert.Heading>
                	</Alert>
                	<BarChart rawData={allRecords} />
                </div>
            </div>
		</React.Fragment>
		)
}