import React, {useEffect, useState, useContext} from 'react';
import Router from 'next/router';
import {InputGroup, Button, FormControl, Form, Card, Row, Col, Alert} from 'react-bootstrap';
import ReactDOM from "react-dom";
import moment from 'moment';

function addTransaction(e){
		e.preventDefault();
		Router.push('/records/addRecord')
	}

export default function Records(){

	const [user, setUser] = useState([]);
	const [selector, setSelector] = useState('All');
	const [search, setSearch] = useState('');
	const [searchResult, setSearchResult] = useState([]);
	const [allRecords, setAllRecords] = useState([]);
	const [allCategory, setAllCategory] = useState([]);
	const [categoryName, setCategoryName] = useState([]);
	const [description, setDescription] = useState([]);
	const [type, setType] = useState([]);
	const [date, setDate] = useState([]);
	const [amount, setAmount] = useState([]);
	const [userDetails, setUserDetails] = useState([]);
	const [balance, setBalance] = useState([]);

	useEffect(() => {
		fetch('https://hidden-temple-78735.herokuapp.com/api/users/details', {
		    headers: {
		        'Authorization': `Bearer ${localStorage.getItem('token')}`
		    }
		})
		.then(res => res.json())
		.then(data => {
			setUser(data.firstName)
		})
	}, [])


	useEffect(()=> {
		fetch('https://hidden-temple-78735.herokuapp.com/api/ledger/', {
		    headers: {
		        'Authorization': `Bearer ${localStorage.getItem('token')}`
		    }
		})
		.then( res => res.json())
		.then(data => {
			setAllRecords(data.reverse())
			const recordDescription = data.map( record => {
				return record.description
			})
			setDescription(recordDescription)

			const recordType = data.map( record => {
				return record.type
			})
			setType(recordType)

			const recordDate = data.map( record => {
				return moment(record.dateOfTransaction).format('MMMM/DD/YYYY')
			})
			setDate(recordDate)

			const recordAmount = data.map( record => {
				return record.amount
			})
			setAmount(recordAmount)
		})
	}, [])

	useEffect(()=> {
		fetch('https://hidden-temple-78735.herokuapp.com/api/categories/', {
		    headers: {
		        'Authorization': `Bearer ${localStorage.getItem('token')}`
		    }
		})
		.then( res => res.json())
		.then(data => {
			setAllCategory(data)
			const name = data.map( record => {
				return {name: record.name}
			})
			setCategoryName(name)
		})
	}, [])


	useEffect(() => {
		fetch('https://hidden-temple-78735.herokuapp.com/api/users/savingsUpdate', {
			method: "POST",
		    headers: {
		        'Authorization': `Bearer ${localStorage.getItem('token')}`
		    }
		})
		.then( res => res.json())
		.then(data => {
			if(data === true){
				fetch('https://hidden-temple-78735.herokuapp.com/api/users/details', {
				    headers: {
				        'Authorization': `Bearer ${localStorage.getItem('token')}`
				    }
				})
				.then(res => res.json())
				.then(data => {
					setUserDetails(data)
					const totalBal = data.savings.toLocaleString(undefined, {maximumFractionDigits:2})

					setBalance(totalBal)
				})
			}
		})
	}, [])

	useEffect(() => {
		const results = allRecords.filter(record => record.description.toLowerCase().includes(search.toLowerCase()));
		setSearchResult(results)
	}, [search])

	const searchRecord = searchResult.map(record =>{
		if(record.type === 'Income'){
			return(
				<Alert variant="success" key={record.description}>
				  <Row>
				  	<Alert.Heading className="col-6 mr-auto">{record.description}</Alert.Heading>
				  	<Alert.Heading className="col-6 text-right">+{record.amount}</Alert.Heading>
				  </Row>
				  <hr />
				  <Row>
				 	<p className="col-6 mr-auto">
				 	  {record.type} ({record.categoryName})
				 	</p>
			  		<p className="col-6 text-right">
			  			{moment(record.dateOfTransaction).format('MMMM/DD/YYYY')}
			  		</p>
				  </Row>
				</Alert>
				)
		}else if(record.type === 'Expense'){
			return(
				<Alert variant="danger" key={record.description}>
				  <Row>
				  	<Alert.Heading className="col-6 mr-auto">{record.description}</Alert.Heading>
				  	<Alert.Heading className="col-6 text-right">-{record.amount}</Alert.Heading>
				  </Row>
				  <hr />
				  <Row>
				 	<p className="col-6 mr-auto">
				 	  {record.type} ({record.categoryName})
				 	</p>
			  		<p className="col-6 text-right">
			  			{moment(record.dateOfTransaction).format('MMMM/DD/YYYY')}
			  		</p>
				  </Row>
				</Alert>
				)
		}
	})


	const everyRecord = allRecords.map(record => {
		if(selector === 'All'){
			if(record.type === 'Income'){
				return(
					<Alert variant="success" key={record.description}>
					  <Row>
					  	<Alert.Heading className="col-6 mr-auto">{record.description}</Alert.Heading>
					  	<Alert.Heading className="col-6 text-right">+{record.amount}</Alert.Heading>
					  </Row>
					  <hr />
					  <Row>
					 	<p className="col-6 mr-auto">
					 	  {record.type} ({record.categoryName})
					 	</p>
				  		<p className="col-6 text-right">
				  			{moment(record.dateOfTransaction).format('MMMM/DD/YYYY')}
				  		</p>
					  </Row>
					</Alert>
					)
			}else if(record.type === 'Expense'){
				return(
					<Alert variant="danger" key={record.description}>
					  <Row>
					  	<Alert.Heading className="col-6 mr-auto">{record.description}</Alert.Heading>
					  	<Alert.Heading className="col-6 text-right">-{record.amount}</Alert.Heading>
					  </Row>
					  <hr />
					  <Row>
					 	<p className="col-6 mr-auto">
					 	  {record.type} ({record.categoryName})
					 	</p>
				  		<p className="col-6 text-right">
				  			{moment(record.dateOfTransaction).format('MMMM/DD/YYYY')}
				  		</p>
					  </Row>
					</Alert>
					)
			}
		}else if(selector === 'Income'){
			if(record.type === 'Income'){
				return(
					<Alert variant="success" key={record.description}>
					  <Row>
					  	<Alert.Heading className="col-6 mr-auto">{record.description}</Alert.Heading>
					  	<Alert.Heading className="col-6 text-right">+{record.amount}</Alert.Heading>
					  </Row>
					  <hr />
					  <Row>
					 	<p className="col-6 mr-auto">
					 	  {record.type} ({record.categoryName})
					 	</p>
				  		<p className="col-6 text-right">
				  			{moment(record.dateOfTransaction).format('MMMM/DD/YYYY')}
				  		</p>
					  </Row>
					</Alert>
					
					)
			}else{
				null
			}
		}else if(selector === 'Expense'){
			if(record.type === 'Expense'){
				return(
					<Alert variant="danger" key={record.description}>
					  <Row>
					  	<Alert.Heading className="col-6 mr-auto">{record.description}</Alert.Heading>
					  	<Alert.Heading className="col-6 text-right">-{record.amount}</Alert.Heading>
					  </Row>
					  <hr />
					  <Row>
					 	<p className="col-6 mr-auto">
					 	  {record.type} ({record.categoryName})
					 	</p>
				  		<p className="col-6 text-right">
				  			{moment(record.dateOfTransaction).format('MMMM/DD/YYYY')}
				  		</p>
					  </Row>
					</Alert>
					)
			}else{
				null
			}
		}
	})




	return (
		<React.Fragment>
			<div className="row d-flex justify-content-center mt-5">
				<div className="col-sm-12 col-md-10 col-lg-8">
					<Alert variant="warning">
					  <Alert.Heading>Hello {user}!</Alert.Heading>
					  <hr />
					  <p className="mb-0">
					    Your current balance is &#8369; {balance}.
					  </p>
					</Alert>
					<Alert variant="secondary">
						<InputGroup>
							<InputGroup.Prepend>
							  <Button variant="primary" onClick={(e) => addTransaction(e)}>Add</Button>
							</InputGroup.Prepend>
							<FormControl value={search} onChange={(e) => setSearch(e.target.value)} placeholder="Search record" aria-describedby="basic-addon1" />
							<Form.Control as="select" value={selector} onChange={(e) => setSelector(e.target.value)} required>
							    <option value="All">All</option>
							    <option value="Income">Income</option>
							    <option value="Expense">Expense</option>
							</Form.Control>
						</InputGroup>
					</Alert>
					{
						search === ''
						? everyRecord
						: searchRecord
					}
				</div>
			</div>
		</React.Fragment>
		)
}