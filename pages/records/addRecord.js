import React, {useState, useContext, useEffect} from 'react';
import { Card, Button, Form, Row, Col, Alert} from 'react-bootstrap';
import Swal from 'sweetalert2';
import Router  from 'next/router';

export default function index(){

    const [allCategoryList, setAllCategoryList] = useState("")
    const [incomeCategoryList, setIncomeCategoryList] = useState("")
    const [expenseCategoryList, setExpenseCategoryList] = useState("")
    const [categoryType, setCategoryType] = useState("")
    const [displayCategory, setDisplayCategory] = useState("")
    const [categoryName, setCategoryName] = useState("")
    const [recordAmount, setRecordAmount] = useState(0)
    const [recordDescription, setRecordDescription] = useState("")


    useEffect(() => {
        fetch('https://hidden-temple-78735.herokuapp.com/api/categories/', {
            headers: {
                'Authorization' : `Bearer ${localStorage.getItem('token')}`
             }
        })
        .then(res => res.json())
        .then(data => {
            setAllCategoryList(data.map((arr) => {
                return {name: arr.name, type: arr.type}
            }))

            let incomeList = data.filter(arr => {
                if(arr.type === "Income"){
                    return arr
                }
            })

            let expenseList = data.filter(arr => {
                if(arr.type === "Expense") {
                    return arr
                }
            })

            setIncomeCategoryList(incomeList.map((arr) => {
                return {name: arr.name}
            }))

            setExpenseCategoryList(expenseList.map((arr) => {
                return {name: arr.name}
            }))
        })
    }, [])

    useEffect(() => {

        function categoryNames(arr) {
            return arr.map( e => {
                return  <option value={e.name} key={e.name}>{e.name}</option>
            })
        }
        if(categoryType === "Income"){
            setDisplayCategory(
                categoryNames(incomeCategoryList)
            )
        }
        if(categoryType === "Expense"){
            setDisplayCategory(
                categoryNames(expenseCategoryList)
            )
        }

        setCategoryName("")

    }, [categoryType])

    function addRecord(e) {
        e.preventDefault()

        fetch('https://hidden-temple-78735.herokuapp.com/api/ledger/', {
            'method' : "POST",
            'headers' : {
                'Authorization' : `Bearer ${localStorage.getItem('token')}`,
                'Content-type': "application/json"
            },
            'body' : JSON.stringify({
                name: categoryName,
                type: categoryType,
                amount: recordAmount,
                description: recordDescription
            })
        })
        .then( res => res.json())
        .then( data => {
            if (data === true) {  
                Swal.fire({
				    icon: "success",
				    title: "New record has been created."
				})
                Router.push('/records')
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Something went wrong!'
                })
            }
        })
    }


    return (
        <React.Fragment>
            <div className="row d-flex justify-content-center mt-5">
                <div className="col-sm-12 col-md-10 col-lg-8">
                    <Alert variant="warning">
                      <Alert.Heading>New Record</Alert.Heading>
                    </Alert>
                    <Alert variant="info">
                        <Alert.Heading>Record Information</Alert.Heading>
                        <hr />
                            <Form onSubmit={(e) => addRecord(e)}>
                                <Form.Group controlId="categoryTame">
                                  <Form.Label>Category Type:</Form.Label>
                                  <Form.Control as="select" value={categoryType} onChange={(e) => setCategoryType(e.target.value)} required>
                                      <option value="" disabled>Select Category Type</option>
                                      <option value="Income">Income</option>
                                      <option value="Expense">Expense</option>
                                  </Form.Control>
                                </Form.Group>
                                <Form.Group controlId="categoryName">
                                  <Form.Label>Category Name:</Form.Label>
                                  <Form.Control as="select" value={categoryName} onChange={(e) => setCategoryName(e.target.value)} required>
                                      <option value="" disabled>Select Category Name</option>
                                       {displayCategory}
                                </Form.Control>
                                </Form.Group>
                                <Form.Group controlId="amount">
                                  <Form.Label>Amount:</Form.Label>
                                  <Form.Control type="number" value={recordAmount} onChange={(e) => setRecordAmount(e.target.value)} required/>
                                </Form.Group>
                                <Form.Group controlId="description">
                                  <Form.Label>Description:</Form.Label>
                                  <Form.Control type="text" placeholder="Enter description" value={recordDescription} onChange={(e) => setRecordDescription(e.target.value)} required/>
                                </Form.Group>
                                <Button variant="primary btn-block" type="submit" id="submitBtn">Submit</Button>
                            </Form>
                    </Alert>
                </div>
            </div>
        </React.Fragment>
    )
}