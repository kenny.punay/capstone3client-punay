import React, {useEffect, useContext} from 'react';
import Router from 'next/router';
import UserContext from './../../UserContext.js';

export default function index(){
	const {unsetUser} = useContext(UserContext)
	
	useEffect(() => {
		unsetUser()
		Router.push('/')
	}, [])

	return null
}
