import React, {useState, useEffect, useContext} from 'react';
import Router from 'next/router';
import Swal from 'sweetalert2';
import {Form, Button, Row, Col, Alert} from 'react-bootstrap';
import UserContext from '../UserContext.js';
import {GoogleLogin} from 'react-google-login';
import Link from 'next/link';

export default function index() {
    const {user, setUser} = useContext(UserContext);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(true);

    function authenticate(e) {
        e.preventDefault();
        fetch('https://hidden-temple-78735.herokuapp.com/api/users/login', {
            method: "POST",
            headers: {
                'Content-Type': "application/json"
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then( res => res.json())
        .then(data => {
            if(data.accessToken){
                localStorage.setItem('token', data.accessToken)
                
                fetch('https://hidden-temple-78735.herokuapp.com/api/users/details', {
                    headers: {
                        Authorization: `Bearer ${data.accessToken}`
                    }
                })
                .then(res => res.json())
                .then(data => {
                    localStorage.setItem('id', data._id)
                    setUser({
                        id: data._id
                    })
                })

                setEmail('');
                setPassword('');

                Swal.fire({
                    icon: "success",
                    title: "Successfully logged in.",
                    text: "Thank you for logging in."
                })
                Router.push('/records')
            }else {
                if(data.error === 'does-not-exist'){
                    Swal.fire({
                    icon: "error",
                    title: "Unsuccessful",
                    text: "User does not exist."
                    })
                }else if(data.error === 'login-type-error'){
                    Swal.fire({
                    icon: "error",
                    title: "Login Type Error",
                    text: "You may have registered through a different login procedure."
                    })
                }else if(data.error === 'incorrect-password'){
                    Swal.fire({
                    icon: "error",
                    title: "Unsuccessful",
                    text: "Incorrect password."
                    })
                }
            }
        })
    }

    useEffect(() => {
        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }

    }, [email, password]);

    function authenticateGoogleToken(res){
        fetch('https://hidden-temple-78735.herokuapp.com/api/users/verify-google-id-token', {
            method: "Post",
            headers: {
                'Content-Type': "application/json"
            },
            body: JSON.stringify({
                tokenId: res.tokenId
            })
        })
        .then(res => res.json())
        .then(data => {
            if(typeof data.accessToken !== 'undefined'){
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken);

                Router.push('/records');
                
            }else{
                if(data.error === 'google-auth-error'){
                    Swal.fire({
                    icon: "error",
                    title: "Google Auth Error",
                    text: "Google authentication procedure failed."
                    })
                }else if(data.error === 'login-type-error'){
                    Swal.fire({
                    icon: "error",
                    title: "Login Type Error",
                    text: "You may have registered through a different login procedure."
                    })
                }
            }
        })
    }

    function retrieveUserDetails(accessToken){
        fetch('https://hidden-temple-78735.herokuapp.com/api/users/details', {
            headers: {
                authorization: `Bearer ${accessToken}`
            }
        })
        .then(res => res.json())
        .then(data => {
            localStorage.setItem('id', data._id);

            setUser({
                id: data._id
            });
        })
    }

    return (
    	<React.Fragment>
    		<div className="row d-flex justify-content-center mt-5">
                <div className="col-sm-12 col-md-10 col-lg-8">
                    <Alert variant="warning">
                      <Alert.Heading>Zuitt Budget Tracking</Alert.Heading>
                      <hr />
                      <p className="mb-0">
                        Login by using your Google Account or your Registered Email.
                      </p>
                    </Alert>
                    <Alert variant="info">
                        <Form onSubmit={(e) => authenticate(e)}>
                            <Form.Group controlId="userEmail">
                                <Form.Label>Email address</Form.Label>
                                <Form.Control 
                                    type="email" 
                                    placeholder="Enter email"
                                    value={email}
                                    onChange={(e) => setEmail(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group controlId="password">
                                <Form.Label>Password</Form.Label>
                                <Form.Control 
                                    type="password" 
                                    placeholder="Password"
                                    value={password}
                                    onChange={(e) => setPassword(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            {isActive 
                                ? <Button variant="primary btn-block" type="submit" id="submitBtn">Login</Button>
                                : <Button variant="danger btn-block" type="submit" id="submitBtn" disabled>Login</Button>
                            }

                            <GoogleLogin clientId="654877691492-bh2m2eur558j7lljvlkf1a4d6m2p302u.apps.googleusercontent.com" onSuccess={authenticateGoogleToken} onFailure={authenticateGoogleToken} cookiePolicy={'single_host_origin'} className="w-100 text-center my-4 d-flex justify-content-center"/>
                            <p className="text-center mt-2">
                            <Link href="/register/">
                                <a>Register here.</a>
                            </Link>
                            </p>
                        </Form>
                    </Alert>
                </div>    
            </div>
    	</React.Fragment>
    )
}
