import React, {useState,useEffect, useContext} from 'react';
import Router from 'next/router';
import {Table, Button, Row, Col, Alert} from 'react-bootstrap';


export default function Categories(){

	const [allCategory, setAllCategory] = useState([]);

	function add(e){
		e.preventDefault();
		Router.push('/categories/addCategory')
	}

	useEffect(()=> {
		fetch('https://hidden-temple-78735.herokuapp.com/api/categories/', {
		    headers: {
		        'Authorization': `Bearer ${localStorage.getItem('token')}`
		    }
		})
		.then( res => res.json())
		.then(data => {
			// console.log(data)
			setAllCategory(data)
		})
	}, [])
	// console.log(allCategory)

	const categories = allCategory.map(category => {
		return(
			<tr key={category.name}>
				<td>{category.name}</td>
				<td>{category.type}</td>
			</tr>
			)
	})

	return (
		<React.Fragment>
			<div className="row d-flex justify-content-center mt-5">
				<div className="col-sm-12 col-md-10 col-lg-8">
					<Alert variant="warning">
					  <Alert.Heading>Categories</Alert.Heading>
					</Alert>	
					<Button className="mb-3" variant="primary" onClick={(e) => add(e)}>Add</Button>
					<Alert variant="secondary">
						<Table striped bordered>
							<thead>
								<tr>
									<th>Category</th>
									<th>Type</th>
								</tr>
							</thead>
							<tbody>
								{categories}
							</tbody>
						</Table>
					</Alert>
				</div>
			</div>
		</React.Fragment>
		)
}

