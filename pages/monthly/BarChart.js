import React, {useState, useEffect} from 'react';
import {Bar} from 'react-chartjs-2';
import moment from 'moment';


export default function BarChart({rawData}){
	// console.log(rawData)

	const [months, setMonths] = useState(['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']);
	const [expensePerMonth, setExpensePerMonth] = useState([]);
	const [incomePerMonth, setIncomePerMonth] = useState([]);

	useEffect(() => {
		setExpensePerMonth(months.map(month => {
			let total = 0;
			rawData.forEach(element => {
				if(element.type === 'Expense'){
					if(moment(element.dateOfTransaction).format('MMMM') === month){
					total += element.amount
				}
				}
			})
			// console.log(total)
			return total
		}))
	}, [rawData])

	useEffect(() => {
		setIncomePerMonth(months.map(month => {
			let total = 0;
			rawData.forEach(element => {
				if(element.type === 'Income'){
					if(moment(element.dateOfTransaction).format('MMMM') === month){
					total += element.amount
				}
				}
			})
			// console.log(total)
			return total
		}))
	}, [rawData])

	const data = {
		labels: months,
		datasets: [{
			label: 'Monthly Expense',
			backgroundColor: 'red',
			borderColor: 'white',
			borderWidth: 1,
			hoverBackgroundColor: 'orange',
			hoverBorderColor: 'black',
			data: expensePerMonth
		}, {
			label: 'Monthly Income',
			backgroundColor: 'blue',
			borderColor: 'white',
			borderWidth: 1,
			hoverBackgroundColor: 'lightBlue',
			hoverBorderColor: 'black',
			data: incomePerMonth
		}]
	}

	const options = {
		scales: {
			yAxes: [{
				ticks: {beginAtZero: true}
			}]
		}
	}

	return(
		<React.Fragment>
			<Bar data={data} options={options}/>
		</React.Fragment>
		)
}
