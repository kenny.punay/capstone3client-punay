import React, {useState, useEffect} from 'react';
import Router from 'next/router';
import {Form, Button, Alert} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function index(){
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [email, setEmail] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");
	const [isActive, setIsActive] = useState(false);

	function registerUser(e) {
		e.preventDefault();

		fetch('https://hidden-temple-78735.herokuapp.com/api/users/emailExist', {
			method: "POST",
			headers: {
				'Content-Type': "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				fetch('https://hidden-temple-78735.herokuapp.com/api/users/', {
					method: "POST",
					headers: {
						'Content-Type': "application/json"
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password1
					})
				})
				.then(res => res.json())
				.then(data => {
					if (data) {
						Swal.fire({
						    icon: "error",
						    title: "Registration failed",
						    text: "Something's Wrong. Please check your inputs."
						})
					}else {
						Swal.fire({
						    icon: "success",
						    title: "Registration complete",
						    text: "Thank you for regestering."
						})
						Router.push('../')
					}
				})
				
			} else {
				Swal.fire({
				    icon: "error",
				    title: "Email already exists",
				    text: "Please use a different email."
				})
			}
		})

		setFirstName('');
		setLastName('');
		setMobileNo('');
		setEmail('');
		setPassword1('');
		setPassword2('');
	}

	useEffect(() => {
		if((firstName !== '' && lastName !== '' && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2) && (mobileNo.length === 11)){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password1, password2]);

	return(
		<React.Fragment>
			<div className="row d-flex justify-content-center mt-5">
	            <div className="col-sm-12 col-md-10 col-lg-8">
	            	<Alert variant="info">
	            		<Form onSubmit={(e)  => registerUser(e)}>
	            			<Form.Group controlId="firstName">
	            				<Form.Label>First name</Form.Label>
	            				<Form.Control type="text" placeholder="Enter First name" value={firstName} onChange={(e) => setFirstName(e.target.value)} required />
	            			</Form.Group>

	            			<Form.Group controlId="lastName">
	            				<Form.Label>Last name</Form.Label>
	            				<Form.Control type="text" placeholder="Enter Last name" value={lastName} onChange={(e) => setLastName(e.target.value)} required />
	            			</Form.Group>

	            			<Form.Group controlId="mobileNo">
	            				<Form.Label>Contact numbet</Form.Label>
	            				<Form.Control type="number" placeholder="Enter contact number" value={mobileNo} onChange={(e) => setMobileNo(e.target.value)} required />
	            				<Form.Text className="text-muted">*We'll never share your contact number with anyone else.</Form.Text>
	            			</Form.Group>

	            			<Form.Group controlId="userEmail">
	            				<Form.Label>Email address</Form.Label>
	            				<Form.Control type="email" placeholder="Enter email" value={email} onChange={(e) => setEmail(e.target.value)} required />
	            				<Form.Text className="text-muted">*We'll never share your email with anyone else.</Form.Text>
	            			</Form.Group>

	            			<Form.Group controlId="password1">
	            				<Form.Label>Password</Form.Label>
	            				<Form.Control type="password" placeholder="Password" value={password1} onChange={(e) => setPassword1(e.target.value)} required />
	            			</Form.Group>

	            			<Form.Group controlId="password2">
	            				<Form.Label>Confirm Password</Form.Label>
	            				<Form.Control type="password" placeholder="Confirm Password" value={password2} onChange={(e) => setPassword2(e.target.value)} required />
	            			</Form.Group>

	            			{
	            				isActive
	            				? <Button variant="primary" type="submit" id="submitBtn">Submit</Button>
	            				: <Button variant="danger" type="submit" id="submitBtn" disabled>Submit</Button>
	            			}	
	            		</Form>
	            	</Alert>
	            </div>
			</div>
		</React.Fragment>
		)
}

