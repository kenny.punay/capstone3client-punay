import React, {useState, useEffect} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/globals.css';
import NavBar from '../components/NavBar.js';
import {Container} from 'react-bootstrap';
import {UserProvider} from '../UserContext.js';

function MyApp({ Component, pageProps }) {
		// console.log(Component)
		// console.log(pageProps)
	  const [user, setUser] = useState({
	  	id: null
	  })

	  const unsetUser = () => {
	  	localStorage.clear();
	  	setUser({
	  		id: null
	  	})
	  }
	  //localStorage can only be access after this component has been rendered. the use effect will only trigger after initial render [].
	  useEffect(() => {
	  	setUser({
	  		id: localStorage.getItem('id')
	  	})
	  }, [])
	  
	  return (
	  	<React.Fragment>
	  		<UserProvider value={{user, setUser, unsetUser}}>
	  			<NavBar/>
	  			<Container>
	  				<Component {...pageProps} />
	  			</Container>
	  		</UserProvider>
	  	</React.Fragment>
	  	)
}

export default MyApp
